--1 int, bigint,tinyint,bit

--int data type with is maximum 10 value
declare @intdatatype int
set @intdatatype=1234567890
select @intdatatype

--bigint data type value more than 10
declare @bigintdatatype bigint
set @bigintdatatype=12345678901234
select @bigintdatatype

-- tinyint data type
declare @tinyintdatatype tinyint
set @tinyintdatatype=255
select @tinyintdatatype
--bit
declare @mybit bit
set @mybit='true'
select @mybit

declare @mybit1 bit
set @mybit1='false'
select @mybit1

--2. decimal, float 
--Decimal data type
--range define
declare @decimaldatatype decimal(20,4)
set @decimaldatatype=1012221.12334312
declare @decimaldatatype1 decimal(10,3)
set @decimaldatatype1=1012221.12334312
-- range not define in decimal
declare @decimaldatatype2 decimal
set @decimaldatatype2=1012221.12334312
select @decimaldatatype,@decimaldatatype1,@decimaldatatype2

--float data type
declare @floatdatatype float
set @floatdatatype=123.235433232276
select @floatdatatype

--3. datetime, date, time, datetime2
-- DateTime
declare @datetimedatatype datetime
set @datetimedatatype=getdate()
select @datetimedatatype

--date
declare @mydate date
set @mydate=getdate()
select @mydate
--time
declare @mytime time
set @mytime=GETDATE()
select @mytime
--datetime2
declare @mydatetime2 datetime2
set @mydatetime2=GETDATE()
select @mydatetime2
select @datetimedatatype,@mydate,@mytime,@mydatetime2

--. char, varchar, nvarchar datatype
--Char data type without range
declare @mychar char
set @mychar='vikas';
select @mychar
--char datatype with range
declare @mychar1 char(10)
set @mychar1='vikas1233@';
select @mychar1
--nchar 
declare @mynchar nchar
set @mynchar='vikas'
select @mynchar

-- varchar without declaring range
declare @myvarchar varchar
set @myvarchar='vikas upadhyay';
select @myvarchar
-- varchar with range
declare @myvarchar1 varchar(10)
set @myvarchar1='vikas upadhyay';
select @myvarchar1
-- varchar with max
declare @myvarchar2 varchar(max)
set @myvarchar2='vikas''upadhyay';
select @myvarchar2
--nvarchar 
declare @mynvarchar nvarchar(10)
set @mynvarchar='vikas upadhyay';
select @mynvarchar

--5. uniqueidentifier
declare @myuniqueidentifier uniqueidentifier
set @myuniqueidentifier=N'0E984725-C51C-4BF4-9960-E1C80E27ABA0wrong' 
select @myuniqueidentifier

--6.Date format -> convert in different formats
declare @mydateformat date
set @mydateformat=GETDATE()
select @mydateformat
select convert(varchar, @mydateformat, 1)
select convert(varchar, getdate(), 2)
select convert(varchar, getdate(), 3)
select convert(varchar, getdate(), 4)
select convert(varchar, getdate(), 5)
select convert(varchar, getdate(), 6)
select convert(varchar, getdate(), 7)

select convert(varchar, getdate(), 9)
select convert(varchar, getdate(), 23)

--Data Conversion: CAST & CONVERT
--Implicit conversion
declare @str varchar(10)
set @str=1
select @str + ' is a string'
declare @datetime varchar(20)
set @datetime=GETDATE()
select @datetime

declare @int int
set @int=255
select @int
declare @age decimal(10,3)
set @age=12
select @age

declare @abc int
set @abc=111.111
select @abc


--explicit conversion
--varchar to int conversion
declare @salary varchar(max)
set @salary='10000'
select Cast(@salary as int)
--select @salary+convert(1000 as int)
--varchar to decimal
declare @salary1 varchar(max)
set @salary1='10000.1111'
select @salary1+Cast(1000 as decimal(10,3))
select @salary1+CONVERT(decimal(10,2),1000)
declare @abc1 decimal(10,2)
set @abc1=111.111
select @abc1+cast(10 as int)
select @abc1+CONVERT(int,10)

declare @name int
set @name=10
select @name+cast(10as varchar(20))
select @name+convert(varchar(20),10)

declare @age1 int
set @age1=12
select cast(@age1 as decimal(10,3))
select CONVERT(decimal(10,2),@age1)

--. Date functions: datediff, dateadd
declare @mydatediff date
set @mydatediff='2011-01-31'
select datediff(month, @mydatediff,'2020-08-12');
select datediff(YEAR, @mydatediff,GETDATE());
select datediff(DAY, @mydatediff,GETDATE());

--DateAdd
declare @currentDate date
set @currentDate=GETDATE()
select @currentDate,DATEADD(day,1,@currentDate)
select @currentDate,DATEADD(month,2,@currentDate)
select @currentDate,DATEADD(year,2,@currentDate)
select @currentDate,DATEADD(week,3,@currentDate)

--String data type functions: LTRIM, RTRIM, LEN etc.
declare @Mystring varchar(max)
set @Mystring='      vikas uapdhyay.   '
select @Mystring
select LEN(@Mystring)
select LTRIM(@Mystring)
select RTRIM(@Mystring)
select REVERSE(@Mystring)
select UPPER(@Mystring);
