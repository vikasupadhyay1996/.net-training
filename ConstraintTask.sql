--Default Constraint,not null Constraint and Identity Constraint
create table StudentTbl(Roll_No int not null identity(101,1) primary key,Name nvarchar(50) not null,Address varchar(100),Age tinyint,State varchar(10) default 'UP')
insert into StudentTbl(Address) values('Noida')--gets error msg because name does not allow null value
insert into StudentTbl(Name,Address,State,Id) values('Vikas','Noida','Delhi',126)
select * from StudentTbl
truncate table StudentTbl
--Unique
insert into StudentTbl(Name,Address,State,Id) values('Vikas','Noida','Delhi',123)
alter table StudentTbl
Add Id int  unique
insert into StudentTbl(Name,Address,State,Id) values('Ram','Noida','Delhi',1234)

--Check Constraint
Create Table StudentInfo(Id int not null primary key identity,Name varchar(100) not null,Age int not null check(Age>=18))
insert into StudentInfo values('Vikas',18)
insert into StudentInfo values('Vikas',19)


select * from StudentInfo
