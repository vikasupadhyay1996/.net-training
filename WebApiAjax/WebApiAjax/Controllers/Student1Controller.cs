﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApiAjax.Models;

namespace WebApiAjax.Controllers
{
    public class Student1Controller : ApiController
    {
        private studentDbEntities db = new studentDbEntities();

        // GET: api/Student1
        public IQueryable<Student1> GetStudent1()
        {
            return db.Student1;
        }

        // GET: api/Student1/5
        [ResponseType(typeof(Student1))]
        public IHttpActionResult GetStudent1(int id)
        {
            Student1 student1 = db.Student1.Find(id);
            if (student1 == null)
            {
                return NotFound();
            }

            return Ok(student1);
        }

        // PUT: api/Student1/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutStudent1(int id, Student1 student1)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != student1.id)
            {
                return BadRequest();
            }

            db.Entry(student1).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Student1Exists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Student1
        [ResponseType(typeof(Student1))]
        public IHttpActionResult PostStudent1(Student1 student1)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Student1.Add(student1);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = student1.id }, student1);
        }

        // DELETE: api/Student1/5
        [ResponseType(typeof(Student1))]
        public IHttpActionResult DeleteStudent1(int id)
        {
            Student1 student1 = db.Student1.Find(id);
            if (student1 == null)
            {
                return NotFound();
            }

            db.Student1.Remove(student1);
            db.SaveChanges();

            return Ok(student1);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Student1Exists(int id)
        {
            return db.Student1.Count(e => e.id == id) > 0;
        }
    }
}