﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EntityLinqCrudOperationDemo.Models;

namespace EntityLinqCrudOperationDemo.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        studentDbEntities student = new studentDbEntities();
        public ActionResult Index()
        {
            var data = student.StudentsInfoes.ToList(); 
            return View(data);
        }
        public ActionResult AddStudent()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddStudent(StudentsInfo model)
        {
           
            student.StudentsInfoes.Add(model);
            student.SaveChanges();

            
            ViewBag.msg = "Data Inserted successfully";
            ModelState.Clear();
           
            return View();

        }
        public ActionResult Detail(int id)
        {
            var getById = student.StudentsInfoes.Single(x => x.Id == id);
            return View(getById);
        }
        public ActionResult EditStudent(int id)
        {
            var getById = student.StudentsInfoes.Single(x => x.Id == id);
            return View(getById);
        }
        [HttpPost]
        public ActionResult EditStudent(int id,StudentsInfo model)
        {
            
            StudentsInfo obj= student.StudentsInfoes.Single(x => x.Id == id);
            obj.FirstName = model.FirstName;
            obj.LastName = model.LastName;
            obj.Address = model.Address;
            obj.Email = model.Email;
            obj.DOB = model.DOB;
            student.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult DeleteStudent(int id)
        {
            StudentsInfo obj = student.StudentsInfoes.Single(x => x.Id == id);
            student.StudentsInfoes.Remove(obj);
            student.SaveChanges();
            return RedirectToAction("Index");
        }


    }
    
}