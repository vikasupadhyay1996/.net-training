﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRUDOpeartionAspDotNetCore.Models;
using CRUDOpeartionAspDotNetCore.StudentServices;
using Microsoft.AspNetCore.Mvc;

namespace CRUDOpeartionAspDotNetCore.Controllers
{
    public class StudentController : Controller
    {
        private StudentService _stuService;
        public IActionResult List()
        {
            _stuService = new StudentService();
            var model = _stuService.GetStudentList();
            return View(model);
        }
        public IActionResult AddStudent()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddStudent(StudentModel model)
        {
            _stuService = new StudentService();
            _stuService.InsertStudent(model);
            ViewBag.msg = "Data Inserted successfully";
            ModelState.Clear();
            //  return RedirectToAction("List");
            return View();

        }
        public IActionResult EditStudent(int Id)
        {
            _stuService = new StudentService();
            var model = _stuService.GetEditById(Id);
            return View(model);
        }
        [HttpPost]
        public IActionResult EditStudent(StudentModel model)
        {
            _stuService = new StudentService();
            _stuService.UpdateStu(model);

            var model1 = _stuService.GetStudentList();
           
            return View("List",model1);
        }
        public IActionResult DeleteStudent(int Id)
        {
            _stuService = new StudentService();
            _stuService.DeleteStu(Id);

            return RedirectToAction("List");
        }
    }
}