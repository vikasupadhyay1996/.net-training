﻿using CRUDOpeartionAspDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDOpeartionAspDotNetCore.StudentServices
{
    public class StudentService
    {
        SqlConnection con = new SqlConnection("Data Source=VIKASHUPADYAYSU\\SQLEXPRESS;Initial Catalog=studentDb;User Id=sa;Password=Vikas123@;");
        private SqlDataAdapter _adapter;
        private DataSet _ds;




        public IList<StudentModel> GetStudentList()
        {
            IList<StudentModel> getStuList = new List<StudentModel>();
            _ds = new DataSet();
            con.Open();
            SqlCommand cmd = new SqlCommand("Select * from StudentsInfo", con);
            con.Close();
            cmd.CommandType = CommandType.Text;
            _adapter = new SqlDataAdapter(cmd);
            _adapter.Fill(_ds);
            if (_ds.Tables.Count > 0)
            {
                for (int i = 0; i < _ds.Tables[0].Rows.Count; i++)
                {
                    StudentModel obj = new StudentModel();
                    obj.Id = Convert.ToInt32(_ds.Tables[0].Rows[i]["Id"]);
                    obj.FirstName = Convert.ToString(_ds.Tables[0].Rows[i]["FirstName"]);
                    obj.LastName = Convert.ToString(_ds.Tables[0].Rows[i]["LastName"]);
                    obj.Address = Convert.ToString(_ds.Tables[0].Rows[i]["Address"]);
                    obj.Email = Convert.ToString(_ds.Tables[0].Rows[i]["Email"]);
                    obj.DOB = Convert.ToDateTime(_ds.Tables[0].Rows[i]["DOB"]);
                    getStuList.Add(obj);


                }
            }
            return getStuList;




        }
        public void InsertStudent(StudentModel model)
        {
            con.Open();

            SqlCommand cmd = new SqlCommand("Insert into StudentsInfo values (@FirstName,@LastName,@Address,@Email,@DOB)", con);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@FirstName", model.FirstName);
            cmd.Parameters.AddWithValue("@LastName", model.LastName);
            cmd.Parameters.AddWithValue("@Address", model.Address);
            cmd.Parameters.AddWithValue("@Email", model.Email); ;
            cmd.Parameters.AddWithValue("@DOB", model.DOB);


            cmd.ExecuteNonQuery();
            con.Close();

        }
        public StudentModel GetEditById(int Id)
        {
            var model = new StudentModel();
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from StudentsInfo where Id=@Id ", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Id", Id);
            _adapter = new SqlDataAdapter(cmd);
            _ds = new DataSet();
            _adapter.Fill(_ds);
            if (_ds.Tables.Count > 0 && _ds.Tables[0].Rows.Count > 0)
            {
                model.Id = Convert.ToInt32(_ds.Tables[0].Rows[0]["Id"]);
                model.FirstName = Convert.ToString(_ds.Tables[0].Rows[0]["FirstName"]);
                model.LastName = Convert.ToString(_ds.Tables[0].Rows[0]["LastName"]);
                model.Address = Convert.ToString(_ds.Tables[0].Rows[0]["Address"]);
                model.Email = Convert.ToString(_ds.Tables[0].Rows[0]["Email"]);
                model.DOB = Convert.ToDateTime(_ds.Tables[0].Rows[0]["DOB"]);
            }
            con.Close();

            return model;
        }
        public void UpdateStu(StudentModel model)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("update StudentsInfo set FirstName=@FirstName,LastName=@LastName,Address=@Address,Email=@Email,DOB=@DOB where Id=@Id", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Id", model.Id);
            cmd.Parameters.AddWithValue("@FirstName", model.FirstName);
            cmd.Parameters.AddWithValue("@LastName", model.LastName);
            cmd.Parameters.AddWithValue("@Address", model.Address);
            cmd.Parameters.AddWithValue("@Email", model.Email); ;
            cmd.Parameters.AddWithValue("@DOB", model.DOB);
            cmd.ExecuteNonQuery();
            con.Close();

        }
        public void DeleteStu(int Id)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("delete from  StudentsInfo where Id=@Id", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Id", Id);
            cmd.ExecuteNonQuery();
            con.Close();

        }
    }
}
