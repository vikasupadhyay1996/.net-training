﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoDotNetCommandClassTask
{
    class ExecuteScalarClass
    {
        public void ExecuteScalar()
        {
            SqlConnection con = new SqlConnection("Data Source=VIKASHUPADYAYSU\\SQLEXPRESS;Initial Catalog=EmployeeDb;User Id=sa;Password=Vikas123@;");
            SqlCommand cmd = new SqlCommand("select Count(*) from tblEmployee", con);
            con.Open();
            int count = Convert.ToInt32(cmd.ExecuteScalar());
            con.Close();
            Console.WriteLine("No of Employee :" + count);

            SqlCommand cmd1 = new SqlCommand("select Max(salary) from tblEmployee", con);
            con.Open();
            //int count1 = Convert.ToInt32(cmd1.ExecuteScalar());
            int maxSalary = Convert.ToInt32(cmd1.ExecuteScalar());
            con.Close();
            Console.WriteLine("Max Salary in Db:" + maxSalary);

            SqlCommand cmd2 = new SqlCommand("select Avg(salary) from tblEmployee", con);
            con.Open();
            //int count1 = Convert.ToInt32(cmd1.ExecuteScalar());
            int avgSalary = Convert.ToInt32(cmd2.ExecuteScalar());
            con.Close();
            Console.WriteLine("Avg salary in Db :" + avgSalary);
            SqlCommand cmd3 = new SqlCommand("select Sum(salary) from tblEmployee", con);
            con.Open();
            //int count1 = Convert.ToInt32(cmd1.ExecuteScalar());
            int totalSalary = Convert.ToInt32(cmd3.ExecuteScalar());
            con.Close();
            Console.WriteLine("Total Salary :" + totalSalary);
        }
    }
}
