﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoDotNetCommandClassTask
{
    class ExecuteNonQueryClass
    {
        public void ExecuteNonQuery()
        {
            SqlConnection con = new SqlConnection("Data Source=VIKASHUPADYAYSU\\SQLEXPRESS;Initial Catalog=EmployeeDb;User Id=sa;Password=Vikas123@;");



           
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("1:Insert\\2:update\\3:delete\\0:exit");
                int choice = int.Parse(Console.ReadLine());


                switch (choice)
                {
                    case 1:
                        SqlCommand cmd = new SqlCommand("insert into tblEmployee values(@name,@gender,@salary,@city,@Department)", con);
                        Console.WriteLine("Enter Name");
                        string name = Console.ReadLine();
                        cmd.Parameters.AddWithValue("@name", name);
                        Console.WriteLine("Enter gender");
                        string gender = Console.ReadLine();
                        cmd.Parameters.AddWithValue("@gender", gender);
                        Console.WriteLine("Enter salary");
                        int salary = int.Parse(Console.ReadLine());
                        cmd.Parameters.AddWithValue("@salary", salary);
                        Console.WriteLine("Enter city");
                        string city = Console.ReadLine();
                        cmd.Parameters.AddWithValue("@city", city);
                        Console.WriteLine("Enter Department");
                        string department = Console.ReadLine();
                        cmd.Parameters.AddWithValue("@Department", department);
                        con.Open();
                        cmd.ExecuteNonQuery();

                        Console.WriteLine("data inserted Successfully!");
                        con.Close();


                        break;
                    case 2:
                        SqlCommand cmd1 = new SqlCommand("update tblEmployee set name=@name where id=@id", con);
                        Console.WriteLine("Enter Name:");
                        string name1 = Console.ReadLine();
                        cmd1.Parameters.AddWithValue("@name", name1);


                        Console.WriteLine("Enter ID:");
                        string Id = Console.ReadLine();
                        cmd1.Parameters.AddWithValue("@id", Id);


                        con.Open();
                        cmd1.ExecuteNonQuery();

                        Console.WriteLine("data updated  Successfully!");
                        con.Close();

                        break;
                    case 3:
                        SqlCommand cmd2 = new SqlCommand("delete from tblEmployee where id=@id", con);
                        Console.WriteLine("Enetr Id");
                        int id = int.Parse(Console.ReadLine());
                        cmd2.Parameters.AddWithValue("@id", id);
                        con.Open();

                        cmd2.ExecuteNonQuery();
                        con.Close();
                        Console.WriteLine("Delete successfully");

                        break;
                    case 0:
                        flag = false;
                        break;
                    default:
                        Console.WriteLine("Please select Valid Choices from the above");
                        break;

                }

            }
           

        }
    }
}
