﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoDotNetCommandClassTask
{
    class Program
    {
        static void Main(string[] args)
        {
            ExecuteReaderClass executeReaderClass = new ExecuteReaderClass();
            ExecuteNonQueryClass executeNonQueryClass = new ExecuteNonQueryClass();
            ExecuteScalarClass executeScalarClass = new ExecuteScalarClass();

            bool flag = true;
            while (flag)
            {
                Console.WriteLine();
                Console.WriteLine("1:Excute Reader\\2:Exicute NonQuery\\3:Execute Scalar\\0:Exit");
                int choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        executeReaderClass.ExicuteReader();

                        break;
                    case 2:
                        executeNonQueryClass.ExecuteNonQuery();
                        break;
                    case 3:
                        executeScalarClass.ExecuteScalar();

                        break;
                    case 0:
                        System.Environment.Exit(0);
                        flag = false;
                        break;
                    default:
                        Console.WriteLine("Please select valid choices");
                        break;
                }

            }

            Console.ReadKey();
        }
    }
}
