--Joins
--inner Joins
create table custmers(custid tinyint primary key identity(1,1) not null,custname varchar(100),custcity varchar(100))
insert into custmers values('vikas','noida')
insert into custmers values('anubhav','delhi')
insert into custmers values('ram','gurugaon')
insert into custmers values('kanika','jaipur')
select * from custmers
create table orders(orderid int not null primary key identity(101,1),custid int ,orderdate date)
insert into orders values(1,'2020/12/09')
insert into orders values(2,'2020/12/08')
insert into orders values(3,'2020/11/09')
insert into orders values(4,'2019/12/09')
insert into orders values(7,'2020/12/09')
select * from orders
--Inner Join
select orders.orderid,custmers.custname
from orders
inner join custmers
on custmers.custid=orders.custid


--left outer join
select orders.orderid,custmers.custname
from orders
left join custmers
on custmers.custid=orders.custid
order by custmers.custname

--right outer join
select orders.orderid,custmers.custname
from orders
right join custmers
on custmers.custid=orders.custid

--full outer join
select orders.orderid,custmers.custname
from orders
full join custmers
on custmers.custid=orders.custid




