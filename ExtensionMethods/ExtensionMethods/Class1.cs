﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionMethods
{
    class Class1
    {
		public void doSomething()
		{
			try
			{
				//Console.WriteLine("Hello");
				int a, b, c;
				a = 10;
				b = 0;
				c = a / b;
				Console.WriteLine(c);
		 	}
			catch (Exception ex)
			{
				if (ex is IndexOutOfRangeException || ex is DivideByZeroException || ex is Exception)
				{
					Console.WriteLine(ex.Message);
				}
			}
		}
	}
}
