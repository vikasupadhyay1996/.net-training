﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtensionMethods;

namespace ExtensionMethods
{
    public static class IntExtensions
    {
        public static bool IsGreaterThan(this int i, int value)
        {
            return i > value;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            int i = 101;

            bool result = i.IsGreaterThan(100);

            Console.WriteLine("Result: {0}", result);

            Class1 class1 = new Class1();
            class1.doSomething();
            Class2 class2 = new Class2();
            string str = "Hello";
            class2.Xyz( ref str);
            Console.WriteLine(str);
            Class3 class3 = new Class3();
            int G;
           Class3.Sum(out G);
            Console.WriteLine("The sum of" +
                " the value is: {0}", G);

            Console.ReadKey();
        }
        }
    }

