﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRUDDemoEFCore.Data;
using Microsoft.AspNetCore.Mvc;

namespace CRUDDemoEFCore.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly DataContext _context = null;
        public IActionResult Index()
        {
            var data = _context.Employees.ToList();
            return View(data);
           
        }
    }
}