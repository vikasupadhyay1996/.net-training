﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApiDemoAspDotNetCore.Models;
using WebApiDemoAspDotNetCore.StudentServices;

namespace WebApiDemoAspDotNetCore.Controllers
{


    [Route("[controller]")]
    [ApiController]

    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }


        //public IEnumerable<WeatherForecast> Get()
        //{
        //    var rng = new Random();
        //    return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        //    {
        //        Date = DateTime.Now.AddDays(index),
        //        TemperatureC = rng.Next(-20, 55),
        //        Summary = Summaries[rng.Next(Summaries.Length)]
        //    })
        //    .ToArray();
        //}


        StudentService studentService = new StudentService();
        [HttpGet]
        public IActionResult GetAllStudent()
        {
            return Ok(studentService.GetStudentList());
        }
        [HttpGet("{id}")]
        public IActionResult GetEditSudent(int id)
        {
            var getData = studentService.GetEditById(id);

            return Ok(getData);
        }


        [HttpPost]
        public IActionResult PostStudent(StudentModel model)
        {
            studentService.InsertStudent(model);
            return Ok();
        }





        public IActionResult PutUpadateSudent(StudentModel model)
        {
            studentService.UpdateStu(model);
            return Ok();
        }
        [HttpDelete("{id}")]
        public IActionResult DeleteStudent(int id)
        {
            studentService.DeleteStu(id);
            return Ok();
        }
    }
}
