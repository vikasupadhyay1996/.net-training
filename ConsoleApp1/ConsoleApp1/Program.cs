﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
          
            bool flag = true;
                while (flag) {
                try { 
                Console.WriteLine("Enter a value you want to convert");

                string input = Console.ReadLine();
                Console.WriteLine("Enter your choice to convert");
                Console.WriteLine("1-int,2-float,3-char,4-bool,5-decimal 0 -exit");
                int choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                            Console.WriteLine(int.Parse(input));
                        break;
                    case 2:
                        Console.WriteLine(Convert.ToSingle(input));
                        break;
                    case 3:
                        Console.WriteLine(char.Parse(input));
                        break;
                    case 4:
                        Console.WriteLine(bool.Parse(input));
                        break;
                    case 5:
                        Console.WriteLine(decimal.Parse(input));
                        break;
                    case 0:
                        System.Environment.Exit(0);
                        flag = false;
                        break;
                        default:
                            Console.WriteLine("Incorrect choices Please select valid choice");
                            
                            break;


                }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
