﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ValidationDemo1.Models;

namespace ValidationDemo1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Index1()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(UserModels obj)
        {
            ViewBag.msg = "Successfully inserted";
            List<UserModels> list = new List<UserModels>();
            list.Add(obj);
            return View(list);


        }
    }
}