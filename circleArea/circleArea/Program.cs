﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace circleArea
{
    
    class Program
    {
        // public static double r;//

        static void Main(string[] args)
        {
            try
            {
                bool flag = true;
                while (flag)
                {
                   
                    Console.WriteLine("Please select the Choice for Calculation:1-perimeter/2-area/0-exit from the program ");
                   int  choice = int.Parse(Console.ReadLine());
                    double r;



                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine("Hello! You are enterning to calculate 'Perimeter' of the Circle");
                            Console.WriteLine("Please Input the radious : ");
                            r = Convert.ToDouble(Console.ReadLine());
                            var circle = new Circle(r);


                            circle.CirclePerimeter();
                            break;

                        case 2:
                            Console.WriteLine("Hello! You are enterning to calculate 'Area' of the Circle");
                            Console.WriteLine("Please Input the radious : ");
                            r = Convert.ToDouble(Console.ReadLine());
                            var circle1 = new Circle(r);


                            circle1.CircleArea();
                            break;
                        case 0:
                            System.Environment.Exit(0);
                            flag = false;

                            break;
                        default:

                            Console.WriteLine("You are not selecting 'Wrong' Number");
                            break;
                    }


                    //  circle.CirclePerimeter();
                    //  circle.CircleArea();

                  
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }
    }
}
