﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace circleArea
{

    class Circle
    {
       public double a;
   
        public double cer_per, cer_area;
        public double PI = 3.14;
        public Circle(double radious)
        {
            a = radious;

        }
        public void CirclePerimeter()
        {
            cer_per = 2 * PI*a ;
            Console.WriteLine("Perimeter of Circle is: "+ cer_per);

        }
        public void CircleArea()
        {
            cer_area = PI * a * a;
            Console.WriteLine("Area of Circle is: {0}", cer_area);

        }


    }
}
