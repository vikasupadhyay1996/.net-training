﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListTask
{
    class Person
    {

       public string Name { get; set; }
       public int Age { get; set; }
        //public Person(string name, int age)
        //{
        //    this.Name = name;
        //    this.Age = age;

        //}

        public void PersonAge()
        {
            List<Person> PersonList = new List<Person>
            {
                new Person{Age=15,Name="Vikas" },
                new Person{Age=60,Name="Ram" },
                new Person{Age=65,Name="Shyam" },
                new Person{Age=62,Name="Mohan" },
                new Person{Age=59,Name="Sohan" },
            };
            //List<Person> PersonList = new List<Person>();
            //PersonList.Add(new Person { Age = 15, Name = "Vikas" });
            //PersonList.Add(new Person { Age = 60, Name = "Ram" });
            //PersonList.Add(new Person { Age = 65, Name = "Shyam" });
            //PersonList.Add(new Person { Age = 62, Name = "Mohan" });
            //PersonList.Add(new Person { Age = 59, Name = "Sohan" });

            foreach (var item in PersonList)
            {
                if (item.Age >= 60)
                {
                    Console.WriteLine(item.Age + "  " + item.Name);
                }
            }

        }


    }
}
