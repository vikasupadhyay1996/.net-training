select * from emptbl
alter table emptbl
add Age tinyint
insert into emptbl values('shivali','QA','Ghaziyabad','shivali@gmail.com',25);
update emptbl 
set Age=24
where id=1
update emptbl 
set Age=24
where id=2
update emptbl 
set Age=23
where id=3
update emptbl 
set Age=25
where Name='shivali'
-- Keyword task
--Where keyword
select * from emptbl
where Department='Dot net';

select * from emptbl
where id=1

select * from emptbl
where age=24

update emptbl set Name='Santosh',Address='Delhi' where id=8;

update emptbl
set Age=25
where id=1

update emptbl
set Name='Vikas Upadhyay'
where Name='Vikas'

delete emptbl
where id=6

-- Order By Keyword
select * from emptbl
order by Name;

select * from emptbl
order by Name desc;

select Name from emptbl
order by Department  ;

select * from emptbl
order by Department  ;

select Address from emptbl
order by Address

select * from emptbl
order by Age desc

select * from emptbl
order by Name,Age;

--Group by keyword
select COUNT (id),age
from emptbl
group by age

select COUNT (id),age
from emptbl
group by age

select COUNT(id),name
from emptbl
group by name

select COUNT (id),age
from emptbl
group by age
order by Count(id) desc

select COUNT (department)
from emptbl
group by Department

select COUNT (department),Department
from emptbl
group by Department

select AVG(id),age
from emptbl
group by age


--Having  keyword
select Count(id),name
from emptbl
group by name
having count(name)>=2


select Count(id),age
from emptbl
group by age
having count(id)>=1

select Count(id),age
from emptbl
group by age
having age>23

-- Distinct keyword
select distinct name from emptbl

select count(distinct Name) from emptbl

select distinct Department,age,name from emptbl

--Like keyword
select * from emptbl
where name like'%v'

select * from emptbl
where name like'v%'

select * from emptbl
where name like'%y'

select * from emptbl
where name like'k%a'

select * from emptbl
where age like 25

--In
select * from emptbl
where name in('vikas upadhyay','anubhav')

select * from emptbl
where age in(23,25)

select * from emptbl
where age not in(23,25)


