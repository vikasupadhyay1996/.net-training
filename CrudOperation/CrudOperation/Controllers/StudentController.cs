﻿using CrudOperation.Models;
using CrudOperation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrudOperation.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        private StudentService _stuService;
        public IEnumerable<StudentsInfo> studentModels(List<StudentsInfo> data)
        {
            var z = new List<StudentsInfo>();
            foreach (var item in data)
            {
                z.Add(item);
            }
            return z.AsEnumerable();

        }
        public ActionResult List()

        {
            var x = new studentDbEntities();
            var res = x.StudentsInfoes.ToList();
            var z = studentModels(res);
          
            return View(z);
            //_stuService = new StudentService();
            //var model = _stuService.GetStudentList();
            //return View(model);
        }
        public ActionResult AddStudent()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddStudent(StudentsInfo model)
        {
            var x = new studentDbEntities();
            x.StudentsInfoes.Add(model);
            x.SaveChanges();

            //_stuService = new StudentService();
            //_stuService.InsertStudent(model);
            ViewBag.msg="Data Inserted successfully";
            //  return RedirectToAction("List");
            return View();

        }
        public ActionResult EditStudent(int Id)
        {
            _stuService = new StudentService();
            var model = _stuService.GetEditById(Id);
            return View(model);
        }
        [HttpPost]
        public ActionResult EditStudent(StudentsInfo model)
        {
            _stuService = new StudentService();
            _stuService.UpdateStu(model);
          
            var model1 = _stuService.GetStudentList();
            return View("List",model1);
        }
        public ActionResult DeleteStudent(int Id)
        {
            var x = new studentDbEntities();
           var abc= x.StudentsInfoes.Find(Id);
            var xyz = x.StudentsInfoes.Where(a => a.Id == Id).Select(y => y.FirstName).FirstOrDefault();
            if (abc==null)
            {

                return View();

            }
            else
            {
                x.StudentsInfoes.Remove(abc);
                x.SaveChanges();
                return RedirectToAction("List");
            }
            //_stuService = new StudentService();
            //_stuService.DeleteStu(Id);

            //return RedirectToAction("List");
        }
    }
}