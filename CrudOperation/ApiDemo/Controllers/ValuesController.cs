﻿using CrudOperation.Models;
using CrudOperation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiDemo.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
        private StudentService _stuService;
        //Get method
        public IList<StudentsInfo> GetList()
        {
            _stuService = new StudentService();
            var model1 = _stuService.GetStudentList();
            return _stuService.GetStudentList();
        }
        public IHttpActionResult PostAddStudent(StudentsInfo model)
        {
            _stuService = new StudentService();
            _stuService.InsertStudent(model);
            // ViewBag.msg = "Data Inserted successfully";
            //  return RedirectToAction("List");
            return Ok(model);

        }
        public IHttpActionResult DeleteStudent(int Id)
        {
            _stuService = new StudentService();
            _stuService.DeleteStu(Id);
            return Ok(Id);
        }
       
        }
}
