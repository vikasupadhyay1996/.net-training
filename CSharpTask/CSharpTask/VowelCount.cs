﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTask
{
    class VowelCount
    {
        public void CountVowel()
        {
            string myStr;
            int i, len, vowelCount;
            Console.WriteLine("Please enter the string");
            myStr = Console.ReadLine();
            vowelCount = 0;
            
            // find length
            len = myStr.Length;
            for (i = 0; i < len; i++)
            {
                if (myStr[i] == 'a' || myStr[i] == 'e' || myStr[i] == 'i' || myStr[i] == 'o' || myStr[i] == 'u' || myStr[i] == 'A' || myStr[i] == 'E' || myStr[i] == 'I' || myStr[i] == 'O' || myStr[i] == 'U')
                {
                    vowelCount++;
                }
                
            }
            Console.WriteLine("Number of vowel:"+vowelCount);
        }
    }
}
