﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTask
{
    class SmallestNumList
    {
        public void DisplayList()
        {
			string[] elements;

			while (true)
			{
				Console.WriteLine("Enter a list of comma separated numbers.");
				var input = Console.ReadLine();

				if (!String.IsNullOrEmpty(input))
				{
					elements = input.Split(',');
					if (elements.Length >= 5)
					{

						break;
					}
					Console.WriteLine("List is less than 5 number");
				}
				else
					Console.WriteLine("List Can not be null! Please try again.");
			}

			var muNum = new List<int>();

			foreach (var number in elements)
			{
				muNum.Add(int.Parse(number));
			}

			var smallestsNum = new List<int>();
			while (smallestsNum.Count < 3)
			{
				var min = muNum[0];
				foreach (var number in muNum)
				{
					if (number < min)
						min = number;
				}
				smallestsNum.Add(min);

				muNum.Remove(min);
			}
			Console.WriteLine("3 Smallest Numbers in the List:");
			foreach (var number in smallestsNum)
			{
				Console.WriteLine(number);
			}
		}
    }
}
