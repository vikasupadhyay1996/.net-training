﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTask
{
    class Program
    {
        static void Main(string[] args)
        {
            UniqueNum uniqueNum = new UniqueNum();
            ListWithoutDuplicates listWithoutDuplicates = new ListWithoutDuplicates();
            SmallestNumList smallestNumList = new SmallestNumList();
            VowelCount vowelCount = new VowelCount();
          
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("Please select Any Of the Following Choices-");
                Console.WriteLine("1:Display 5 Unique Number!");
                Console.WriteLine("2:Display List Without Duplicates!");
                Console.WriteLine("3:Diplay List Of Three Smallest Number!");
                Console.WriteLine("4:Count Vowel In Input String!");
                Console.WriteLine("0:Exit From the Program!");

                if (long.TryParse(Console.ReadLine(), out long choice))
                {
                    switch (choice)
                    {
                        case 1:
                            uniqueNum.SortedNumber();

                            break;
                        case 2:
                            listWithoutDuplicates.DisplayUniqueNumber();

                            break;
                        case 3:
                            smallestNumList.DisplayList();
                            

                            break;
                        case 4:
                            vowelCount.CountVowel();

                            break;
                       

                        case 0:
                            System.Environment.Exit(0);
                            flag = false;
                            break;
                        default:
                            Console.WriteLine("Please select Valid Choices from the Follwoing List");
                            break;

                    }


                }
                else
                {
                    Console.WriteLine("please enetr only Integer");
                    continue;
                }
              
              
                Console.ReadLine();



            }
        }
    }
}
