﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using ConsumeWebApiDemo1.Models;

namespace ConsumeWebApiDemo1.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            IEnumerable<StudentModel> student = null;
            using (var client=new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62516/api/");
                var responseTask = client.GetAsync("student");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readJob = result.Content.ReadAsAsync<IList<StudentModel>>();
                    readJob.Wait();
                    student = readJob.Result;
                }
                else
                {
                    //return the error code here
                    student = Enumerable.Empty<StudentModel>();
                    ModelState.AddModelError(string.Empty,"Error occured !") ;
                }

            }
                return View(student);
        }
        public ActionResult Create()
        {
            return View();

        }
        [HttpPost]

        public ActionResult Create(StudentModel student)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62516/api/Student");
                var postTask = client.PostAsJsonAsync<StudentModel>("student", student);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");
                return View(student);
            }
        }
        public  ActionResult Edit(int id)
        {
            StudentModel model = null;
            using (var client=new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62516/api/");
                var responseTask = client.GetAsync("Student/"+id.ToString());
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<StudentModel>();
                    readTask.Wait();
                    model = readTask.Result;

                }
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(StudentModel student)
        {
            using (var client=new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62516/api/Student");
                var putTask = client.PutAsJsonAsync<StudentModel>("student", student);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");
                return View(student);
            }
        }
        public ActionResult Delete(int id)
        {
            using (var client=new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62516/api/");
                var deleteTask = client.DeleteAsync("Student/" + id.ToString());
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
        }

    }
}