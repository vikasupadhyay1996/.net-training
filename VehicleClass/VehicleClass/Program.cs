﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleClass
{
    class Program
    {
        static void Main(string[] args)
        {
          
            Vehicle myObj = new Vehicle("Red",200);
            Car car = new Car("Blue",4,70);
            Bike bike = new Bike("Black",2,60);
            car.CalculateTollAmount();
            bike.CalculateTollAmount();
          //  myObj.noOfWheels = 4;
          //  Console.WriteLine("No of Wheels:"+myObj.noOfWheels);
            myObj.color = "Red";
            Console.WriteLine("Vehicle color:"+myObj.color);
            myObj.Start();
           
            myObj.Stop();
            myObj.SpeedUp(100);
            myObj.Display();
            myObj.Painter();
            car.ChangeSeatCover();
            bike.ChangeSeatCover();
            Console.ReadKey();
        }
    }
}
