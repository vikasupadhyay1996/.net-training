﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleClass
{
    public class Vehicle:IPainter
    {
        private string vehicleName;
        public string color { get; set; }
        //public int noOfWheels { get; set; }
        enum NoOfWheel{
            Vehicle=4,
           

        }

        // public const int maxSpeed = 200;
        private readonly int maxSpeed;
        //public int MaxSpeed
        //{
        //    get
        //    {
        //        return maxSpeed;
        //    }
        //}
        public Vehicle()
        {
            string name = "Ford";
            vehicleName = name;

            Console.WriteLine(vehicleName);
        }

        public Vehicle( string VehicleColor,int VehicleSpeed)

        {
            int myNum=(int)NoOfWheel.Vehicle;
           // noOfWheels = wheel;
            color = VehicleColor;
            maxSpeed = VehicleSpeed;
            

            Console.WriteLine("Vehicle Wheels:" + myNum +";"+ " Vehicle color:" + color +";"+ " Vehicle Max Speed:" + maxSpeed);


        }
        public void Start()
        {
            Console.WriteLine("Vehicle is start");

        }
        public void Stop()
        {
           
            Console.WriteLine("Vehicle is stop");
          
        }
        public void SpeedUp(int a)
        {
            Console.WriteLine("Currently speed of this car is: " + a);
        }
        public void Display()
        {
            Console.WriteLine("maximum speed of this car is:" + maxSpeed);

        }
        public void Painter()
        {
            Console.WriteLine("Vehicle is painted with red color");
        }
    }
   sealed class Car : Vehicle,IChangeSeatCover
    {
      //  int maxSpeed;


       
        public  Car(string name,int noOfwheel, int VehicleSpeed):base( name,VehicleSpeed)
        {
            
           

        }
        public void CalculateTollAmount()
        {
            Console.WriteLine("Calculate toll amount for car");
        }
        public void ChangeSeatCover()
        {
            Console.WriteLine("Changing the Seat Car Cover");
        }


    }
   sealed class Bike : Vehicle,IChangeSeatCover
    {
        //int maxSpeed=40;



        public Bike(string name, int noOfWheels, int VehicleSpeed) : base(name, VehicleSpeed)
        {
           
         


        }

        public void CalculateTollAmount()
        {
            Console.WriteLine("Calculate toll amount for Bike");
        }
        public void ChangeSeatCover()
        {
            Console.WriteLine("Changing the Seat Bike Cover");
        }



    }
}
