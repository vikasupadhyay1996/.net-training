﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrudDemo1EFCore.Data;
using CrudDemo1EFCore.Models;
using Microsoft.AspNetCore.Mvc;

namespace CrudDemo1EFCore.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly DataContext _context = null;
        public EmployeeController(DataContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var data = _context.Employees.ToList();
            return View(data);

        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(Employee model)
        {

            _context.Employees.Add(model);
            _context.SaveChanges();


            ViewBag.msg = "Data Inserted successfully";
            ModelState.Clear();

            return View();

        }
        public IActionResult Detail(int id)
        {
            var getById = _context.Employees.Single(x => x.Id == id);
            return View(getById);
        }
        public IActionResult Edit(int id)
        {
            var getById = _context.Employees.Single(x => x.Id == id);
            return View(getById);
        }
        [HttpPost]
        public IActionResult Edit(int id, Employee model)
        {

            Employee obj = _context.Employees.Single(x => x.Id == id);
            obj.Name = model.Name;
            obj.Age = model.Age;
            obj.Salary = model.Salary;
          
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
        public IActionResult Delete(int id)
        {
            Employee obj = _context.Employees.Single(x => x.Id == id);
            _context.Employees.Remove(obj);
           _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}