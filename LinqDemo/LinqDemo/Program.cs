﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqDemo
{
    public class Student
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = { "vikas", "anubhav", "kanika", "avneesh", "sanjay", "mukesh" };
            var MyLinq = from name in names
                         where name.Contains('v')
                         select name;
            foreach (var item in MyLinq)
            {
                Console.WriteLine(item);
            }
            IList<Student> StudentsList = new List<Student>()
            {
                new Student(){StudentId=1,StudentName="Vikas",Age=18},
                 new Student(){StudentId=2,StudentName="Anubhav",Age=17},
                  new Student(){StudentId=3,StudentName="Kanika",Age=16 },
                   new Student(){StudentId=4,StudentName="Avneesh",Age=19},
                    new Student(){StudentId=5,StudentName="Ram",Age=15},
            };
            var teenAgerStudent = from s in StudentsList
                                  where s.Age > 12 && s.Age < 18
                                  select s;
            var shortList = from s in StudentsList orderby s.StudentName select s;
            Console.WriteLine("Teen age Students:");

            foreach (var item in teenAgerStudent)
            {
                Console.WriteLine(item.StudentName);
            }
            Console.WriteLine("Sorting Name in Ascending order");
            foreach (var a in shortList)
            {
                Console.WriteLine(a.StudentName);
            }
            Console.WriteLine("Name with Decending order");
            shortList = from s in StudentsList orderby s.StudentName descending select s;
            foreach (var b in shortList)
            {
                Console.WriteLine(b.StudentName + " " + b.Age + " " + b.StudentId);
            }
            Console.WriteLine("Name with Decending order2");
            shortList = StudentsList.OrderBy(s => s.StudentName);
            foreach (var abc in shortList)
            {
                Console.WriteLine(abc.StudentName);
            }

            Console.WriteLine("Linq with labda expression");
            Student[] studentArray = {
                    new Student() { StudentId = 1, StudentName = "John", Age = 18 } ,
                    new Student() { StudentId = 2, StudentName = "Steve",  Age = 21 } ,
                    new Student() { StudentId = 3, StudentName = "Bill",  Age = 25 } ,
                    new Student() { StudentId = 3, StudentName = "Bill",  Age = 29 } ,
                    new Student() { StudentId = 4, StudentName = "Ram" , Age = 20 } ,
                    new Student() { StudentId = 5, StudentName = "Ron" , Age = 31 } ,
                    new Student() { StudentId = 6, StudentName = "Chris",  Age = 17 } ,
                    new Student() { StudentId = 7, StudentName = "Rob",Age = 19  } ,
                     new Student() { StudentId = 8, StudentName = "Rob",Age = 19  } ,
                };
            Student[] teenAgeStudent = studentArray.Where(s => s.Age > 12 && s.Age < 20).ToArray();
            foreach (var item in teenAgeStudent)
            {
                Console.WriteLine(item.StudentId + " " + item.StudentName);
            }

            //Then by
            Console.WriteLine("Then by!------------");
            var thenByResult = studentArray.OrderBy(s => s.StudentName).ThenBy(s => s.Age);
            foreach (var thenby in thenByResult)
            {
                Console.WriteLine(thenby.StudentName + " " + thenby.Age);

            }
            Console.WriteLine("Group By-------------------");
            var groupedResult = studentArray.GroupBy(s => s.Age);

            foreach (var ageGroup in groupedResult)
            {
                Console.WriteLine("Age Group: {0}", ageGroup.Key);  //Each group has a key 

                foreach (Student s in ageGroup)  //Each group has a inner collection  
                    Console.WriteLine("Student Name: {0}", s.StudentName);
            }

            List<int> intergerList = new List<int>()
            {
               1,2,3,4,5,6,7,8,9,0
            };
            var myObj = from obj in intergerList where obj > 5 select obj;
            foreach (var item in myObj)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }
    
    }
}
