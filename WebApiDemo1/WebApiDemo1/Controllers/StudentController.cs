﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiDemo1.Models;
using WebApiDemo1.StudentServices;

namespace WebApiDemo1.Controllers
{
    public class StudentController : ApiController
    {
        //Get
        StudentService studentService = new StudentService();
        public IHttpActionResult GetAllStudent()
        {
            return Ok(studentService.GetStudentList());
        }
        public IHttpActionResult PostStudent(StudentModel model)
       {
           studentService.InsertStudent(model);
           return Ok();
      }
        public IHttpActionResult PutEditSudent(int id)
        {
            studentService.GetEditById(id);
            return Ok();
       }
        public IHttpActionResult PutUpadateSudent(StudentModel model)
        {
            studentService.UpdateStu(model);
            return Ok();
        }
        public IHttpActionResult DeleteStudent(int id)
        {
            studentService.DeleteStu(id);
            return Ok();       
        }
}
}
