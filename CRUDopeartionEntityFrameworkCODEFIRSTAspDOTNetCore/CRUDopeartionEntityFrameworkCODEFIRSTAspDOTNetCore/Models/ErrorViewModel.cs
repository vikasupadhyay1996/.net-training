using System;

namespace CRUDopeartionEntityFrameworkCODEFIRSTAspDOTNetCore.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
