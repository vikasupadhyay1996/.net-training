﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StopWatch
{
	class Program
	{
		static void Main(string[] args)
		{

			string input;
			StopWatch stopWatch = new StopWatch();
			bool flag = true;
			while (flag)
			{
				Console.WriteLine("1:Start watch ");
				Console.WriteLine("2:Stop Watch");
				Console.WriteLine("0:Exit");

				bool isParsable = int.TryParse(Console.ReadLine(), out int choice);
				if (isParsable)
				{
					switch (choice)
					{
						case 1:
							Console.WriteLine("Type'start' to Start StopWatch");
							input = Console.ReadLine();


							if (input.ToLower() == "start")
							{
								stopWatch.Start();
							}
							else
							{
								Console.WriteLine("U can not 'start' StopWatch without Starting ");
							}

							break;
						case 2:
							Console.WriteLine("Type 'stop' To Stop StopWatch");
							input = Console.ReadLine();

							if (input.ToLower() == "stop")
							{

								stopWatch.Stop();

							}
							else if (input.ToLower() == "start")
							{
								Console.WriteLine("StopWatch is Already running");

							}
							else
							{
								Console.WriteLine("Invalid string");
							}
							break;
						case 0:
							System.Environment.Exit(0);
							flag = false;
							break;
						default:
							Console.WriteLine("Invalid choice!");
							break;

					}

				}
				else
				{
					Console.WriteLine("Only Integer Value Allowed!");
				}

				//var input = Console.ReadLine();
				//if (input == "start")
				//{
				//	stopWatch.Start();
				//}
				//else
				//{

				//	while (Console.ReadLine() != "start")
				//	{
				//		Console.WriteLine("U cant stop without starting the watch..");
				//	}

				//	stopWatch.Start();

				//}

				//Console.WriteLine("Type 'stop' to Stop the Watch");
				//input = Console.ReadLine();
				//if (input == "stop")
				//{
				//	stopWatch.Stop();


				//}
				//else
				//{
				//	Console.WriteLine("Type stop to stop to stop the watch..");
				//	while (Console.ReadLine() != "stop")
				//	{
				//		Console.WriteLine("Type stop to stop th watch..");
				//	}
				//	stopWatch.Stop();


				Console.ReadKey();
				}
				

			
			
			
        }
	}
}
