﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StopWatch
{
	class StopWatch
	{
        public DateTime StartTime { get; set; }
        public DateTime StopTime { get; set; }

        public bool IsRunning { get; set; }


        public void Start()
        {
            try
            {
                if (!IsRunning)
                {
                    IsRunning = true;
                    StartTime = DateTime.Now;
                    Console.WriteLine("Stop watch Running! ");

                }
                else
                {
                    throw new InvalidOperationException("Stopwatch is already running");
                }
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);

            }


        }
        public void Stop()
        {
            try
            {
                if (IsRunning)
                {
                    IsRunning = false;
                    StopTime = DateTime.Now;

                    Console.WriteLine("You stopped after: {0}", StopTime - StartTime);
                    Console.WriteLine();
                }
                else
                {
                    throw new InvalidOperationException("Stopwatch can't be stopped in starting: start it first.");
                }
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);
            }

        }
        //public DateTime StartTime { get; set; }
        //public DateTime StopTime { get; set; }

        //public bool IsRunning { get; set; }


        //public void Start()
        //{
        //    if (!IsRunning)
        //    {
        //        IsRunning = true;
        //        StartTime = DateTime.Now;
        //        Console.WriteLine("Stop watch Running!");

        //    }
        //    else
        //    {
        //        throw new InvalidOperationException("Stopwatch is already running");
        //    }

        //}
        //public void Stop()
        //{
        //    if (IsRunning)
        //    {
        //        IsRunning = false;
        //        StopTime = DateTime.Now;

        //        Console.WriteLine("You stopped after:{0}",StopTime - StartTime);
        //    }
        //    else
        //    {
        //        throw new InvalidOperationException("Stopwatch can't be stopped in starting: start it first.");
        //    }

        //}
    }
}
