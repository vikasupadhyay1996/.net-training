--Primary key
create table Students(Roll_No int not null identity(101,1) primary key,Name nvarchar(50),Address varchar(100),Age tinyint)
insert into Students values('vikas','Mirzapur',24)
insert into Students values('anubhav','Meerut',24)
insert into Students values('kanika','Bhirwada',23)
insert into Students values('shivali','Noida',25)
insert into Students values('shyam','Delhi',27)
truncate table Students
alter table  Students
alter column Age int not null
drop table Students
select * from Students

--foreign key
 create table StudentCourse(Course_id int  primary key identity,Roll_No int foreign key REFERENCES Students(Roll_No))
 insert into StudentCourse values(101)
  insert into StudentCourse values(101)
   insert into StudentCourse values(102)
    insert into StudentCourse values(103)
	select * from StudentCourse

	select StudentCourse.Course_id,Students.Name
	from StudentCourse
	inner join Students on StudentCourse.Roll_No=Students.Roll_No