﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    [Route("")]
    public class EmployeeController : Controller
    {
        // GET: Employee
        [Route("Employee/TestDemo")]
        public ActionResult TestDemo()
        {
            HttpCookie cookie = new HttpCookie("cookie_name", "cookie_value");
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Add(cookie);
          

            string name;
            ViewData["Name"] = "THis is view data";
            if (TempData.ContainsKey("name"))
                name = TempData["name"].ToString();

           cookie = HttpContext.Request.Cookies.Get("cookie_name");
            return View("TestDemo");
        }

        //string query
        public string Index(string id,string name)
        {
            return "ID =" + id + "<br /> Name=" + name+ "<br/>"+"Multiple parameter query string";

        }

    }
}
