﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
  
    public class HomeController : Controller
    {
        // GET: Home
        [Route("Home")]

        public ActionResult Index()
        {

            ViewBag.Name = "This view bag";
            

            return View("Index");

        }
        [Route("")]
        [Route("Home/HomeInfo")]
        public ActionResult HomeInfo()
        {
            return View("HomeInfo");
        }
        [Route("Home/PartialPage")]
        public PartialViewResult PartialPage()
        {
            return PartialView("_PartialPage1");
        }
        public ContentResult Contact()
        {
            ViewBag.messege = "Your contact ";
            return Content("Index");
        }
        public ViewResult Result()
        {
            ViewBag.msg = "This is view result";
            return View("Index");
        }
        public JsonResult WelcomeNote()
        {
            bool isAdmin = false;
         
            string output = isAdmin ? "Welcome to the Admin User" : "Welcome to the User";

            return Json(output, JsonRequestBehavior.AllowGet);
        }

    }
}