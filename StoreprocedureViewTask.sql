-- CTE
with cte as
(select top 5 id from tblEmployee)
select * from tblEmployee where Id in (select * from cte)

--Store procedure

create procedure sp_selectAllEmp
as
select * from emp
go;
exec sp_selectAllEmp



select * from tblEmployee
select * from emptbl
begin transaction
delete from emptbl where Age=24
commit;
begin transaction
delete from emptbl where Age=24
rollback;

create procedure allemp @city varchar(20)
as
select * from tblEmployee where city=@city
go

exec allemp @city='Mujffarpur'

--Views
create view cityemp as
select name,gender
from tblEmployee
select * from  cityemp


create view salarycollection as
select name,gender,salary
from tblEmployee

select * from salarycollection


--User defined function
create function GetAllEmpAge(@Age int)
returns table
as 
return
Select * from emptbl where Age=@Age

select * from GetAllEmpAge(25)





