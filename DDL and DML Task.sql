create database EmployeeDb;
use EmployeeDb;
create table emptbl(id int not null primary key identity(1,1),
Name varchar(100),
Department varchar(100),
Address varchar(200));
insert into emptbl values('vikas','Dot Net','Noida');
insert into emptbl values('kanika','Dot Net','Rajasthan');
insert into emptbl values('anubhav','Dot Net','Meerut');
insert into emptbl values('shivali','QA','Ghaziyabad');
insert into emptbl values('shivali','QA','Ghaziyabad');

select * from emptbl;
select distinct Department from emptbl;
select * from emptbl where Department='Dot Net';
select * from emptbl order by Name,Address;
select * from emptbl order by Name desc;

update emptbl set Name='Santosh',Address='Delhi' where id=5;
delete emptbl where Name='Santosh';
alter table emptbl add Email varchar(100);
insert into emptbl values('shivali','QA','Ghaziyabad','shivali@gmail.com');


create database personDb;
create table  persontbl(Id int not null primary key identity(101,1),
                            FirstName varchar(100), 
							LastName varchar(100),
							Age int 
);
insert into  persontbl values('vikas',
                                    'upadhyay',
									24
                                 );
								 insert into  persontbl values('kanika',
                                    'ranka',
									23
                                 );
								  insert into  persontbl values('anubhav',
                                    'sagar',
									25
                                 );
select * from persontbl;
alter table persontbl 
Add Email varchar(200);
alter table persontbl drop column Email;
delete persontbl where Id=102;
delete persontbl;
truncate table persontbl;
drop table persontbl;

create procedure SelectAllEmployee
as
select * from emptbl
go;
exec SelectAllEmployee;







