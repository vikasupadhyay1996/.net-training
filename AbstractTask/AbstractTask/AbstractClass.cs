﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractTask
{
  abstract  class AbstractClass
    {
        private string vehicleName;
        public  int maxSpeed;

        public string color { get; set; }
        public int noOfWheels { get; set; }

        //public AbstractClass()
        //{
        //    string name = "Ford";
        //    vehicleName = name;

        //    Console.WriteLine(vehicleName);
        //}
        //public AbstractClass(string VehicleColor, int wheel, int VehicleSpeed)

        //{
        //    noOfWheels = wheel;
        //    color = VehicleColor;
        //    maxSpeed = VehicleSpeed;


        //    Console.WriteLine("Vehicle Wheels:" + noOfWheels + ";" + " Vehicle color:" + color + ";" + " Vehicle Max Speed:" + maxSpeed);


        //}


        public abstract void Start();
        public abstract void Stop();

        public abstract void SpeedUp(int a);
        public abstract void CalculateTollAmount();
       



    }
    class Car : AbstractClass
    {
        public Car(string color,int noOfWheels,int maxspeed)
        {
            Console.WriteLine("Vehicle Wheels:" + noOfWheels + ";" + " Vehicle color:" + color + ";" + " Vehicle Max Speed:" + maxspeed);
        }
      
        public  override void Start()
        {
            Console.WriteLine("Car is start");
        }
        public override void Stop()
        {
            Console.WriteLine("Car is Stop");
        }
        public override void SpeedUp(int a)
        {
            Console.WriteLine("Currently speed of this car is: " + a);
        }
        public override void CalculateTollAmount()
        {
            Console.WriteLine("Calculate toll amount for Car");
        }
    }
   class Bike : AbstractClass
    {
        public Bike(string color, int noOfWheels, int maxspeed)
        {
            Console.WriteLine("Vehicle Wheels:" + noOfWheels + ";" + " Vehicle color:" + color + ";" + " Vehicle Max Speed:" + maxspeed);
        }

        public override void Start()
        {
            Console.WriteLine("Bike is start");
        }
        public override void Stop()
        {
            Console.WriteLine("Bike is Stop");
        }
        public override void SpeedUp(int a)
        {
            Console.WriteLine("Currently speed of this Bike is: " + a);
        }
        public override void CalculateTollAmount()
        {
            Console.WriteLine("Calculate toll amount for Bike");
        }

    }
}
