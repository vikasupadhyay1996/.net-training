﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Car myCar = new Car("Red",4,150);
           
            myCar.Start();
            myCar.Stop();
            myCar.SpeedUp(100);
            myCar.CalculateTollAmount();
            Bike myBike = new Bike("Blue", 2, 90);
            myBike.Start();
            myBike.Stop();
            myBike.SpeedUp(60);
            myBike.CalculateTollAmount();

            Console.ReadKey();
        }
    }
}
