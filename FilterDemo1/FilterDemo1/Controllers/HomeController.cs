﻿using FilterDemo1.Common;
using System;
using System.Web.Mvc;

namespace filterDemo1.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        [OutputCache(Duration = 10)]

        public string Index1()
        {
            return DateTime.Now.ToString("T");
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        [HandleError(View = "Error.cshtml")]
        public ActionResult Contact()
        {
            //ViewBag.Message = "Your contact page.";
            //return view();

            return Content("Opps somthing went wrong");
        }
        [OutputCache(Duration = 10)]
        public ActionResult Filter()
        {
            ViewBag.Message = "This your filter page";
            return View();
        }
        [Authorize]
        public ActionResult Secured()
        {
            ViewBag.Message = "This is secured filter page";
            return View();
        }
        [HandleError]
        public ActionResult Index2()
        {
            throw new NullReferenceException();
        }
        [TrackExecutionTime]
        public string Index3()
        {
            return "Index Action Invoked";
        }

        [TrackExecutionTime]
        public string Welcome()
        {
            return ("Exception ocuured");
        }


    }
}