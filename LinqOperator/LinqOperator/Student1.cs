﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqOperator
{
    class Student1
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }
        public static List<Student1> GetAllStudents()
        {
            List<Student1> studentList = new List<Student1>
            {
                new Student1{StudentId=101,StudentName="vikas",Age=24},
                 new Student1{StudentId=102,StudentName="vikas",Age=24},
                  new Student1{StudentId=103,StudentName="vikas",Age=24},
                   new Student1{StudentId=104,StudentName="vikas",Age=24},
                    new Student1{StudentId=105,StudentName="vikas",Age=24},
                     new Student1{StudentId=106,StudentName="vikas",Age=24},
                      new Student1{StudentId=107,StudentName="vikas",Age=24},
                       new Student1{StudentId=108,StudentName="vikas",Age=24},
                     
            };
            return studentList;
        }

    }
}
