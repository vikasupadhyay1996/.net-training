﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LinqOperator
{
    class Employee
    {
        public int EmpId { get; set; }
        public String EmpName { get; set; }
        public int Age { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }

    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> EmpList = new List<Employee>()
                {
                    new Employee(){EmpId=1,EmpName="vikas",Age=24,DepartmentId=1 },
                    new Employee(){EmpId=2,EmpName="anubhav",Age=25,DepartmentId=1 },
                    new Employee(){EmpId=3,EmpName="kanika",Age=23,DepartmentId=1 },
                    new Employee(){EmpId=4,EmpName="shivali",Age=24,DepartmentId=2 },
                    new Employee(){EmpId=5,EmpName="Nidhi",Age=29,DepartmentId=3 },
                    new Employee(){EmpId=6,EmpName="ram",Age=17,DepartmentId=4 },
                    new Employee(){EmpId=7,EmpName="shyam",Age=20 }
                };
            List<Employee> DepartmentList = new List<Employee>()
            {
                new Employee(){DepartmentId=1,DepartmentName="Dot net"},
                new Employee(){DepartmentId=2,DepartmentName="QA"},
                new Employee(){DepartmentId=3,DepartmentName="HR"},
                new Employee(){DepartmentId=4,DepartmentName="It"}
            };
            //Filtering operotor
            //Query Syntax
            Console.WriteLine("Qury syntax Linq");

            string[] names = { "vikas", "shivali", "anubav", "kanika", "avneesh" };
            var name = from a in names where a.Contains("v") select a;
            Console.WriteLine("Print Whoose contain V char:");
            foreach (var item in name)
            {

                Console.WriteLine(item);

            }

            name = from a in names where a.StartsWith("k") && a.EndsWith("a") select a;
            Console.WriteLine("Start with K and end with A:");
            foreach (var item in name)
            {
                Console.Write(item);
            }

            //Method syntax
            Console.WriteLine("Method syntax or Lambda:");
            var myName = names.Where(myobj => myobj.Contains("k"));
            foreach (var item in myName)
            {
                Console.WriteLine(item);
            }
            List<int> integer = new List<int>() {
            1,2,3,4,5,10,20,0,50,76,80,100};
            List<int> myInt = integer.Where(i => i > 5 && i < 76).ToList();
            foreach (var item in myInt)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("Print Emp whoose contain Dot net");
            var myEmp = EmpList.Where(e => e.DepartmentId.Equals(1));
            foreach (var item in myEmp)
            {
                Console.WriteLine(item.EmpId + " " + item.EmpName + " " + item.Age);
            }
            var teen = EmpList.Where(e => e.Age < 18);
            foreach (var item in teen)
            {
                Console.WriteLine(item.EmpName + " " + item.DepartmentId);
            }

            //Join Opeartor
            //Quary syntax
            //Inner Join
            var myJoin = from j in DepartmentList
                         join e in EmpList on j.DepartmentId equals e.DepartmentId
                         select new { e.EmpId, e.EmpName, j.DepartmentName };

            Console.WriteLine("Join Operation:");
            foreach (var item in myJoin)
            {
                Console.WriteLine(item.EmpId + " " + item.EmpName + " " + item.DepartmentName);
            }

            myJoin = from j in DepartmentList
                     join
                    e in EmpList on j.DepartmentId equals e.DepartmentId
                     select new { e.EmpId, e.EmpName, j.DepartmentName };

            Console.WriteLine("Join Operation:");
            foreach (var item in myJoin)
            {
                Console.WriteLine(item.EmpId + " " + item.EmpName + " " + item.DepartmentName);
            }

            //Agreegation function
            int min = integer.Min();
            Console.WriteLine("Minimum number in the List is:" + min);

            int max = integer.Max();
            Console.WriteLine("Maximum number in the List is:" + max);

            int sum = integer.Sum();
            Console.WriteLine("Sum of the list is:" + sum);

            int count = integer.Count();

            Console.WriteLine("List Count:" + count);
            double Average = integer.Aggregate((a, b) => a + b);
            Console.WriteLine("Average number:" + Average);
            Average = integer.Average();
            Console.WriteLine("Average Number1:" + Average);


            //Sorting opearator
            Console.WriteLine("acending order:");
            var mySort = integer.OrderBy(a => a);
            foreach (var item in mySort)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("decending  order:");
            mySort = integer.OrderByDescending(a => a);
            foreach (var item in mySort)
            {
                Console.WriteLine(item);
            }

            var Mysort = EmpList.OrderByDescending(o => o.EmpName);
            foreach (var item in Mysort)
            {
                Console.WriteLine(item.EmpName);

            }
            Console.WriteLine("Query Syntax:");
            Mysort = from o in EmpList orderby o.EmpName select o;
            foreach (var item in Mysort)
            {
                Console.WriteLine(item.EmpName);
            }
            //Concatatination
            string[] array1 = { "a", "b", "c", "d" };

            string[] array2 = { "c", "d", "e", "f" };
            var result = array1.Concat(array2);
            //foreach loop will print the value of result  
            foreach (var item in result)

            {

                Console.WriteLine(item);
            }

            //set opearation 
            string[] count1 = { "India", "USA", "UK", "Australia", "India" };
            string[] count2 = { "India", "Canada", "UK", "China" };
            result = count1.Union(count2);
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("Intersect:");
            result = count1.Intersect(count2);
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("Distinct:");
            result = count1.Distinct();
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
            //projection operator
            List<string> phrases = new List<string>() { "an apple a day", "the quick brown fox" };

            var query = from phrase in phrases
                        from word in phrase.Split(' ')
                        select word;

            foreach (string s in query)
                Console.WriteLine(s);
            //oftype 
            Console.WriteLine("Oftype:");
            IList mixedList = new ArrayList();
            mixedList.Add(1);
            mixedList.Add(2);
            mixedList.Add("One");
            mixedList.Add("Second");
            mixedList.Add(new Student() { StudentId = 1, StudentName = "Vikas" });
            var stringResult1 = from l in mixedList.OfType<int>() select l;
            var intResult = from l in mixedList.OfType<string>() select l;
            var objResult = from l in mixedList.OfType<Student>() select l;
            foreach (var item in stringResult1)
            {
                Console.WriteLine(item);
            }
            foreach (var item in intResult)
            {
                Console.WriteLine(item);
            }
            foreach (var item in objResult)
            {
                Console.WriteLine(item.StudentName);
            }

            //select keyword
            Console.WriteLine("Select keyword:");
            var selectResult = from s in EmpList select s.EmpName;
            foreach (var item in selectResult)
            {
                Console.WriteLine(item+" " +item.Length);
            }

            //all operator
            Console.WriteLine("All operator :");
            bool areAllStudentsTeenAger = EmpList.All(s => s.Age > 12 && s.Age < 20);


            Console.WriteLine(areAllStudentsTeenAger);
            areAllStudentsTeenAger = EmpList.All(s => s.Age > 12 && s.Age < 30);


            Console.WriteLine(areAllStudentsTeenAger);
            IList<int> intList = new List<int>() { 10, 21, 30, 45, 50, 87 };

            IList<string> strList = new List<string>() { "One", null, "Three", "Four", "Five" };


            Console.WriteLine("1st Element in intList: {0}", intList.ElementAt(0));
            Console.WriteLine("1st Element in strList: {0}", strList.ElementAt(0));

            Console.WriteLine("2nd Element in intList: {0}", intList.ElementAt(1));
            Console.WriteLine("2nd Element in strList: {0}", strList.ElementAt(1));

            Console.WriteLine("3rd Element in intList: {0}", intList.ElementAtOrDefault(2));
            Console.WriteLine("3rd Element in strList: {0}", strList.ElementAtOrDefault(2));

            Console.WriteLine("1st Element in intList: {0}", intList.FirstOrDefault());

            //Skip operator
            IList<string> strList1 = new List<string>() { "One", "Two", "Three", "Four", "Five" };

           result = strList1.Skip(2);

            foreach (var str in result)
                Console.WriteLine(str);

            //take operator
            Console.WriteLine();
            var newList = strList1.Take(2);

            foreach (var str in newList)
                Console.WriteLine(str);
            //Conversion 
            Console.WriteLine("strList type: {0}", strList.GetType().Name);

            string[] strArray = strList1.ToArray<string>();// converts List to Array

            Console.WriteLine("strArray type: {0}", strArray.GetType().Name);
            //let keyword
            Console.WriteLine("let keyword");
            var upercaseStudentNames = from s in EmpList
                                       let upercaseStudentName = s.EmpName.ToUpper()
                                        where upercaseStudentName.StartsWith("v")
                                        select upercaseStudentName;

            foreach (var item in upercaseStudentNames)
            {
                Console.WriteLine(item);
            }

            //into keyword
            var teenAgerEmp = from s in EmpList
                                   where s.Age > 12 && s.Age < 20
                                   select s
                                into teenEmp
                                   where teenEmp.EmpName.StartsWith("r")
                                   select teenEmp;


            foreach (var emp in teenAgerEmp)
            {
                Console.WriteLine(emp.EmpName);
            }

            //Except method
            IList<string> strList2 = new List<string>() { "One", "Two", "Three", "Four", "Five" };
            IList<string> strList3 = new List<string>() { "Four", "Five", "Six", "Seven", "Eight" };

            result = strList2.Except(strList3);

            foreach (string str in result)
                Console.WriteLine(str);

            //skipwhile
            Console.WriteLine("Skip While :");

            var resultList = strList1.SkipWhile(s => s.Length < 4);

            foreach (string str in resultList)
                Console.WriteLine(str);
            //deffered exicution
            Console.WriteLine("Deffered Exicution:");
            IEnumerable<Employee> defResult = from d in EmpList
                                       where d.Age == 24
                                       select d;
            EmpList.Add(new Employee() {EmpId=15,EmpName="Vikas Upadhyay",Age=24 });
            foreach (Employee def in defResult)
            {
                Console.WriteLine(def.EmpName+" "+def.EmpId);
            }
            //immediate eXICUTION
            Console.WriteLine("immediate Exicution:");
            IEnumerable<Employee> immeResult = (from d in EmpList
                                              where d.Age == 24
                                              select d).ToList();
            EmpList.Add(new Employee() { EmpId = 16, EmpName = "Vikas kumar Upadhyay", Age = 24 });
            foreach (Employee imme in immeResult)
            {
                Console.WriteLine(imme.EmpName + " " + imme.EmpId);
            }
            //Conversion opearatp
            Console.WriteLine("Conversion Opeartor:");
            int[] num = {1,2,3,5,78,2 };
            List<int> num1 = (from n in num orderby n select n).ToList();
            foreach (var item in num1)
            {
                Console.WriteLine(item);
            }

            List<string> countries = new List<string> { "India","USA","UK"};
            string[] countries1 = countries.ToArray();
            foreach (var i in countries1)
            {
                Console.WriteLine(i);
            }

           

            //paging skip in take
            do
            {
                IEnumerable<Student1> listStudents = Student1.GetAllStudents();
                Console.WriteLine("Please enter Page Number:1,2,3 or 4");
                int pageNumber = 0;
                if (int.TryParse(Console.ReadLine(), out pageNumber))
                {
                    if (pageNumber >= 1 && pageNumber <= 4)
                    {
                        int pageSize = 2;
                        IEnumerable<Student1> pageResult = listStudents.Skip((pageNumber - 1) * pageSize).Take(pageSize);
                        Console.WriteLine();
                        Console.WriteLine("Displaying page number" + pageNumber);
                        foreach (Student1 s in pageResult)
                        {
                            Console.WriteLine(s.StudentId + " " + s.StudentName + " " + s.Age);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Only in between 1 to 4 is valid");
                    }



                }
                else
                {
                    Console.WriteLine("Please Enter only integer in between 1 to 4");
                }
            } while (1 == 1);
            Console.ReadKey();
           




        }
    }

    internal class Student
    {
        public Student()
        {
        }

        public int StudentId { get; set; }
        public int StudentID { get; internal set; }
        public string StudentName { get; set; }
    }
}
