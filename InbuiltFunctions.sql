--string function
declare @mystring varchar(100)
set @mystring='    vikas upadhyay.    '
select @mystring

select LEN(@mystring)

select TRIM(@mystring)

select LTRIM(@mystring);

select RTRIM(@mystring);

select UPPER(@mystring);

select REPLACE(@mystring,'vikas','anubhav');

select REPLICATE(' vikas ',5)

select REVERSE(@mystring)

select REPLICATE(name,2)
from emptbl
select UPPER(name)
from emptbl
select LEN(name)
from emptbl
select trim(name)
from emptbl
select UPPER(name),REVERSE(name),LEN(name)
from emptbl

--date time function
select getdate() 
select DATEPART(YEAR,1)
select DATEADD(DAY,1,GETDATE())
select DATEADD(YEAR,1,GETDATE())
select DATEADD(MONTH,1,GETDATE())
select DATEADD(WEEK,1,GETDATE())
select DATEDIFF(YEAR,'2018-12-09',GETDATE())
select DATEDIFF(MONTH,'2018-12-09',GETDATE())
select DATEDIFF(WEEK,'2018-12-09',GETDATE())
select DATEDIFF(DAY,'2018-12-09',GETDATE())

select CONVERT(varchar(20),GETDATE(),1);
select CONVERT(varchar(20),GETDATE(),10);
select CONVERT(varchar(20),GETDATE(),110);
select CONVERT(varchar(20),GETDATE(),105);
select CONVERT(varchar(20),GETDATE(),111);

--math function
select CEILING(12.27) as ceilivalue
select FLOOR(12.27) as floorvalue
select ROUND(12.487313,2)
select AVG(age) from emptbl
select max(age) from emptbl
select MIN(age) from emptbl
select SQUARE(10)
select SUM(id) from emptbl
select PI()


--Conversion Functions
select CONVERT(int,25)
select CONVERT(varchar(20),50)
select CONVERT(decimal(10,3),25)
select CONVERT(decimal(10,2),25.1)
select CAST(20.5 as int)
select CAST(20 as decimal(10,2))
select cast(20as varchar(10))
select CAST('2020/12/09' as datetime)
select CAST('2020/12/09' as date)

