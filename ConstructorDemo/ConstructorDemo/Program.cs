﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructorDemo
{
    class A
    {
      public A()
        {
            Console.WriteLine("Constructor Class A running first ");
        }
         static A()
        {
            Console.WriteLine("This is static Class A Constructor");
        }
      public  void Abc()
        {
            Console.WriteLine("A class Method");
        }
       
    }
    class B : A
    {
        public B()
        {
           Console.WriteLine("Constructor Class B running second");
        }
        static B()
        {
            Console.WriteLine("This is static Class B constructor");
        }
       public void Xyz()
        {
            Console.WriteLine("This is B class Method");
        }
      
    }
    class Program
    {
        static void Main(string[] args)
        {

            B b = new B();
            b.Abc();
            b.Xyz();


        
           
            
           
            
            Console.ReadKey();
        }
    }
}
