﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackTask
{
    class Program
    {
        static void Main(string[] args)
        {
            var stack = new Stack();


            bool flag = true;
            while (flag)
            {
                Console.WriteLine("1:Push in Stack");
                Console.WriteLine("2:Pop in Stack");
                Console.WriteLine("3:Display in Stack");
                Console.WriteLine("4:Clear all the value in Stack");
                Console.WriteLine("5:Exit From the Program");
                if (long.TryParse(Console.ReadLine(), out long choice))
                {


                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine("Enter a Value to Push in Stack");
                            object obj = Console.ReadLine();

                            stack.Push(obj);


                            break;
                        case 2:

                            stack.Pop();

                            break;
                        case 3:

                            stack.Display();
                            break;
                        case 4:
                            stack.Clear();
                            break;
                        case 5:
                            flag = false;
                            System.Environment.Exit(0);

                            break;
                        default:
                            Console.WriteLine("Invalid choice! Please Choose Valide Choices From the Following List");
                            break;

                    }
                }
                else
                {
                    Console.WriteLine("Please enter only Interger Value");
                }
            }
            Console.ReadLine();

        }
    }
}
