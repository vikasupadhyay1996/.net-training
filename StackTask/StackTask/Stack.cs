﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackTask
{
    internal class Stack
    {
        private object _object;
        //  private List<object> list = new List<object>();
        Stack<object> stack = new Stack<object>();


        internal void Push(object obj)
        {
            _object = obj;
            try
            {
                if (_object == null || _object == "")
                {
                    throw new InvalidOperationException("Cannot use 'Push' if object is null.");
                }
                else
                {
                    
                    stack.Push(obj);
                    Console.WriteLine("You Pushed a value In Stack: " + _object);
                }



            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);
            }


        }
        internal void Pop()
        {
            try
            {
                if (stack.Count == 0)
                {
                    throw new InvalidOperationException("Cannot use 'Pop' if Stack is 'Empty'.");
                }
                else
                {

                    stack.Pop();
                   
                 


                }
            }

            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);
            }



           
        }

        internal void Clear()
        {
            try
            {
                if (stack.Count == 0)
                {
                    throw new InvalidOperationException("Cannot use 'Clear' if Stack is empty.");
                }
                else
                {
                    stack.Clear();
                }
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);
            }




        }

        public void Display()
        {
            try
            {
                if (stack.Count == 0)
                    throw new InvalidOperationException("Stack is empty.");
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);
            }
            Console.WriteLine("Stack Value:");
            foreach (var s in stack)
            {
                Console.WriteLine(s);

            }
            Console.WriteLine();
        }
    }
}
