﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ConsumeWebApiDemoAspDotNetCore.Models;
using Microsoft.AspNetCore.Mvc;

namespace ConsumeWebApiDemoAspDotNetCore.Controllers
{
    public class StudentController : Controller
    {
        public IActionResult Index()
        {
            IEnumerable<StudentModel> student = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62220/");
                var responseTask = client.GetAsync("weatherforecast");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readJob = result.Content.ReadAsAsync<IList<StudentModel>>();
                    readJob.Wait();
                    student = readJob.Result;
                }
                else
                {
                    //return the error code here
                    student = Enumerable.Empty<StudentModel>();
                    ModelState.AddModelError(string.Empty, "Error occured !");
                }

            }
            return View(student);
        }
        public IActionResult Create()
        {
            return View();

        }
        [HttpPost]

        public ActionResult Create(StudentModel student)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62220/weatherforecast");
                var postTask = client.PostAsJsonAsync<StudentModel>("weatherforecast", student);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");
                return View(student);
            }
        }

        public IActionResult Edit(int id)
        {
            StudentModel model = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62220/");
                var responseTask = client.GetAsync("weatherforecast/" + id.ToString());
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<StudentModel>();
                    readTask.Wait();
                    model = readTask.Result;

                }
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(StudentModel student)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62220/weatherforecast");
                var putTask = client.PutAsJsonAsync<StudentModel>("weatherforecast", student);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");
                return View(student);
            }
        }
        public IActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62220/");
                var deleteTask = client.DeleteAsync("weatherforecast/" + id.ToString());
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
        }
    }
}